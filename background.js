
function toggleBookmark() {
  if (currentBookmark) {
    //browser.bookmarks.remove(currentBookmark.id);
  } else {
    //browser.bookmarks.create({title: currentTab.title, url: currentTab.url});
  }
}
//browser.browserAction.onClicked.addListener(toggleBookmark);

async function init(tabs) {
  browser.storage.local.get("serverUrl").then((profileObj)=> {
    var serverUrl = profileObj.serverUrl;
    if (typeof serverUrl === "undefined") {
      console.log("serverUrl not set")
    } else {
      console.log(serverUrl)
    }
  });

  function isSupportedProtocol(urlString) {
    var supportedProtocols = ["https:", "http:", "ftp:", "file:"];
    var url = document.createElement('a');
    url.href = urlString;
    return supportedProtocols.indexOf(url.protocol) != -1;
  }

  function updateTab(tabs) {
    if (tabs[0]) {
      currentTab = tabs[0];
      if (isSupportedProtocol(currentTab.url)) {
        var searching = browser.bookmarks.search({url: currentTab.url});
        searching.then((bookmarks) => {
          currentBookmark = bookmarks[0];
          updateIcon();
        });
      } else {
        console.log(`Bookmark it! does not support the '${currentTab.url}' URL.`)
      }
    }
  }


  function makeIndent(indentLength) {
    return ".".repeat(indentLength);
  }

  function logItems(bookmarkItem, indent) {
    if (bookmarkItem.url) {
      console.log(makeIndent(indent) + bookmarkItem.url);
    } else {
        console.log(makeIndent(indent) + "Folder " + bookmarkItem.title);
        indent++;
    }
    if (bookmarkItem.children) {
      for (child of bookmarkItem.children) {
        logItems(child, indent);
      }
    }
    indent--;
  }

  function logTree(bookmarkItems) {
    logItems(bookmarkItems[0], 0);
    
  }

  function onRejected(error) {
    console.log(`An error: ${error}`);
  }

  async function hendelBookmarks(bookmarkItems){
    //logTree(bookmarkItems);
    console.log(bookmarkItems);

    let BookmarksBar = null;
    let browserType= await GetBrowserType();
    if(browserType=="Chrome"){
      BookmarksBar = bookmarkItems[0].children[0];
    }else{
      if(browserType=="Firefox"){
        BookmarksBar = bookmarkItems[0].children[1];
      }else{
        console.log("unknown browser");
        return("unknown browser");
      }
    }
    logItems(BookmarksBar, 0);
  }

  browser.bookmarks.getTree().then(hendelBookmarks, onRejected);

/*
  var json = JSON.stringify({
    uri: "http://192.168.88.250:8080/add",
    title: "adder"
  })
;
  var xhr = new XMLHttpRequest();
  xhr.open("POST", "http://192.168.88.250:8080/add");
  xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
  xhr.send(json);
*/
}

function hendelBookmarksTree(tree){
  var ws;
    var print = function(message) {
        var d = document.createElement("div");
        d.innerHTML = message;
        output.appendChild(d);
    };
    
        if (ws) {
            return false;
        }
        ws = new WebSocket("ws:\/\/127.0.0.1:8080\/echo");
        ws.onopen = function(evt) {
            print("OPEN");
        }
        ws.onclose = function(evt) {
            print("CLOSE");
            ws = null;
        }
        ws.onmessage = function(evt) {
            print("RESPONSE: " + evt.data);
        }
        ws.onerror = function(evt) {
            print("ERROR: " + evt.data);
        }
    
        if (!ws) {
            return false;
        }
        print("SEND: " + input.value);
        ws.send(input.value);

        if (!ws) {
            return false;
        }
        ws.close();
}

function handleCreated(id, bookmarkInfo) {
  console.log(`New bookmark ID: ${id}`);
  console.log(`New bookmark URL: ${bookmarkInfo.url}`);
}
browser.bookmarks.onCreated.addListener(handleCreated);

function handleChanged(id, changeInfo) {
  console.log("Item: " + id + " changed");
  console.log("Title: " + changeInfo.title);
  console.log("Url: " + changeInfo.url);
}
browser.bookmarks.onChanged.addListener(handleChanged);

function handleRemoved(id, removeInfo) {
  console.log("Item: " + id + " removed");
  console.log("Title: " + removeInfo.node.title);
  console.log("Url: " + removeInfo.node.url);
}
browser.bookmarks.onRemoved.addListener(handleRemoved);

function handleMoved(id, moveInfo) {
  console.log("Item: " + id + " moved");
  console.log("Old index: " + moveInfo.oldIndex);
  console.log("New index: " + moveInfo.index);
  console.log("Old folder: " + moveInfo.oldParentId);
  console.log("New folder: " + moveInfo.parentId);
}
browser.bookmarks.onMoved.addListener(handleMoved);


async function GetBrowserType() {

  try{
    var gettingInfo = await browser.runtime.getBrowserInfo();
    console.log(gettingInfo);
    return(gettingInfo.name);
  }catch(e){
    let userAgent = navigator.userAgent;
    let isFirefox = userAgent.indexOf('Firefox') !== -1;
    if (isFirefox) {
        console.log("FirefoxFirefox");
        return("FirefoxFirefox");
    }else{
      let isChrome = userAgent.indexOf('Chrome') !== -1;
      if(isChrome){
        console.log("Chrome");
        return("Chrome");
      }else{
        console.log("unknown browser");
        return("unknown browser");
      }
    }
  }
}
GetBrowserType();

init();

