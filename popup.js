
function saveURLserver() {
    url = document.getElementById("server").value;
    console.log(url);
    
      let settingItem = browser.storage.local.set({"serverUrl": url});
      settingItem.then(()=>{
          console.log('Value is set to ' + url);
      });
}

function requestPermissions(){

  function onResponse(response) {
    if (response) {
      console.log("Permission was granted");
    } else {
      console.log("Permission was refused");
    }
    return browser.permissions.getAll();  
  }

 // prom.then((settingItem)=>{
    //var permissionsToRequest = {
      //origins: ["https://"+settingItem.serverUrl+"/"]
    //}
    const permissionsToRequest = {
     //origins: ["https://"+document.getElementById("server").value +"/"]
      origins: [document.getElementById("server").value]
    }
    
    browser.permissions.request(permissionsToRequest)
      .catch((erro)=>{
        console.log(erro);
      })
      .then(onResponse)
      .then((currentPermissions) => {
      console.log(`Current permissions:`, currentPermissions);
    });
 // });

}

document.addEventListener('DOMContentLoaded', function () {
  document.getElementById("button").onclick = saveURLserver;
  document.getElementById("permis").addEventListener("click", requestPermissions);


  browser.storage.local.get("serverUrl")
  .then((url)=>{ document.getElementById("server").value = url.serverUrl });

});
